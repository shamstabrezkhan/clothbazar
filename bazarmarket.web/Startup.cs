﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(bazarmarket.web.Startup))]
namespace bazarmarket.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
